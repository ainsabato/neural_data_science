### Course material for day 1.

Tutors, please upload here only the final material before the class.

---------
### NOTES

Order of the files for today:

1. 1\_CourseIntro.pdf
2. 2\_IntroNeuroscience.pdf
3. intro\_probability\_and\_statistics.ipynb
4. predictive\_models\_and\_biomarkers.ipynb
5. load\_data.ipynb
6. BasicPython.ipynb


