### README

This folder is meant for the tutors to upload the presentations and the Jupyter Notebooks that are still in progress. The final course material will be placed in the main folders of equivalent name.