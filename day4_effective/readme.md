### Course material for day 4.

Everything you always wanted to know about effective connectivity (and never dare to ask).

We will fit dynamic models to time series to estimate directed connectivity from observed activity. 

---------
### NOTES

- 1. conn_estim_MAR_PC.ipynb
- 2. conn_detect_MAR_Granger.ipynb

Exercises in 'Extra/':

- fMRI: pyMOU_Simulation_Estimation.ipynb and pyMOU_EC_Estimation.ipynb
- EEG: ana_EEG.ipynb
