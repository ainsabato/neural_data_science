Repository for the course material:

# Hands-on course on neural data science

held at the **[XIII MESIO UPC-UB Summer School, Barcelona](https://mesioupcub.masters.upc.edu/en/xiii-summer-school-2019)**,

**When:** July 1-5, 2019. Daily from 15:00 – 18:00.

**Where:** Pau Gargallo, 14, 08028 Barcelona. Faculty of Mathematics and Statistics, Polytechnic University of Catalonia. Rooms PC1 or PC2.

![UPC logo](Images/Logo_UPC.png)

**Course tutors:**

- [Andrea Insabato](https://andreainsabato.eu) (Center for Brain and Cognition, Pompeu Fabra University) 
- [Adrià Tauste](https://www.upf.edu/web/adria-tauste) (Barcelona Beta Brain Research Center)
- [Matthieu Gilson](https://matthieugilson.eu) (Center for Brain and Cognition, Pompeu Fabra University)
- [Gorka Zamora-López](http://www.zamora-lopez.xyz) (Center for Brain and Cognition, Pompeu Fabra University) 


----------------------

### NOTES:

- Course materials will be updated daily. Please check the corresponding folders every day before the class.