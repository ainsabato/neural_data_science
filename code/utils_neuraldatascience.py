import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.patches import Patch

def cohens_d(group1, group2):
    """
    Calculated effect size for the difference between two groups as Cohen's d.
    This method assumes that the two groups are independent (unpaired) and that the population variances are equal.
    
    PARAMETERS
    ----------
    group1 : 1D list or np.array
        observations from group1
    group2 : 1D list or np.array
        observations from group1
    
    RETURNS
    -------
    cohens_d: float
    """
    mean1 = np.mean(group1)
    mean2 = np.mean(group2)
    std1 = np.std(group1)
    std2 = np.std(group2)
    count1 = len(group1)
    count2 = len(group2)
    dof = (count1 + count2 - 2)
    pooled_std = np.sqrt(((count1 - 1) * std1 ** 2 + (count2 - 1) * std2 ** 2) / dof)
    cohens_d = abs(mean1 - mean2) / pooled_std
    return cohens_d

def pred_model_example(X, y):
    """
    This is an example of a predictive model.
    The function wraps definition, training and test of a logistic regression classifier.
    
    PARAMETERS
    ----------
    X: 2D array
        design matrix with samples on rows and features on columns
    y: 1D array
        labels to be predicted
    
    RETURNS
    -------
    scores: 1D array
        prediction accuracy on each CV fold
    """
    model = LogisticRegression(solver='lbfgs')
    folds = 5
    kf = KFold(n_splits=folds, shuffle=True)

    score = np.zeros([folds])
    i = 0  # counter for repetitions
    for train_idx, test_idx in kf.split(X):  # cross-validation
        model.fit(X[train_idx, :], y[train_idx])
        score[i] = model.score(X[test_idx, :], y[test_idx])
        i+=1
    return score

def show_kFoldCV(N, k):
    """
    graphically represents k-fold CV
    """
    X = np.arange(N)
    kf = KFold(n_splits=k)
    fig, ax = plt.subplots(ncols=1, nrows=k)
    i = 0
    for train, test in kf.split(X):
        ax[i].scatter(X[train], np.repeat(1, len(X[train])), marker='s', s=20000/N**2, c='pink', label='train')
        ax[i].scatter(X[test], np.repeat(1, len(X[test])), marker='s', s=20000/N**2, c='green', label='test')
        ax[i].set_yticks([])
        ax[i].set_xticks([])
        sns.despine(left=True)
        ax[i].set_ylabel('fold %i' %(i+1), rotation=0, ha='right')
        i += 1
    ax[i-1].legend(frameon=False, bbox_to_anchor=(1.04, 1))
    plt.show()
    return fig, ax

def plot_cv_indices(cv, X, y, group, ax, n_splits, lw=10):
    """From sklearn docs. Create a sample plot for indices of a cross-validation object."""

    cmap_cv = plt.cm.coolwarm
    cmap_data = plt.cm.Paired
    # Generate the training/testing visualizations for each CV split
    for ii, (tr, tt) in enumerate(cv.split(X=X, y=y, groups=group)):
        # Fill in indices with the training/test groups
        indices = np.array([np.nan] * len(X))
        indices[tt] = 1
        indices[tr] = 0

        # Visualize the results
        ax.scatter(range(len(indices)), [ii + .5] * len(indices),
                   c=indices, marker='_', lw=lw, cmap=cmap_cv,
                   vmin=-.2, vmax=1.2)

    # Plot the data classes and groups at the end
    ax.scatter(range(len(X)), [ii + 1.5] * len(X),
               c=y, marker='_', lw=lw, cmap=cmap_data)

    ax.scatter(range(len(X)), [ii + 2.5] * len(X),
               c=group, marker='_', lw=lw, cmap=cmap_data)

    # Formatting
    ax.set_title('{}'.format(type(cv).__name__), fontsize=15)
    return ax