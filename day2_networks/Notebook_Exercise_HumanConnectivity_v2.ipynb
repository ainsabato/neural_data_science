{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "XII Summer School in Statistics and Operation Research, July 1-5, 2018.   \n",
    "Universitat Politècnica de Catalunya, Barcelona, Spain.\n",
    "\n",
    "\n",
    "# Exercises (Day-2): Analysis of human structural connectivity\n",
    "#### Gorka Zamora-López, Ph.D.\n",
    "\n",
    "In this exercise, we will reproduce the analysis done during the tutorial, applied to the connectome of cats but using connectivity data from humans. The data consists of structural connectivity matrices and resting-state BOLD signals for 20 healthy subjects. In this case, the network consist of a parcellation of the human cortex into **68 Regions of Interest** (ROIs), usually refered as the [Desikan parcellation](https://surfer.nmr.mgh.harvard.edu/fswiki/CorticalParcellation).\n",
    "\n",
    "This is part of the ARCHI dataset, collected at NeuroSpin in Paris and made publicly available by the Human Brain Project. For reference see:\n",
    "\n",
    "- Y. Assaf, et al., \"The CONNECT project: Combining macro- and micro-structure\", Neuroimage 80 (2013) 273–282. doi:10.1016/j.neuroimage.2013.05.055.\n",
    "- D. Duclap, \"Towards a super-resolution CONNECT/ARCHI atlas of the white matter connectivity\", Proc. Int. Soc. Magn. Reson. Med. 21th Annual Meeting (2013) 3153.\n",
    "\n",
    "<img src=\"Images/HBPlogo_smaller.png\" style=\"float:left; width:254px; heigth:50px \" alt=\"HBP logo\" />\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "### Import fundamental libraries\n",
    "\n",
    "Let's get started. For that, the very first thing we need is to load the fundamental libraries we need to work. We will make an absolut import of *NumPy*.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from timeit import default_timer as timer\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "from numpy import*\n",
    "from galib import*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Structural Connectivity –––––––––––––––––––––––––––––––––––––––––––––––\n",
    "### Load the data, clean and explore basic features\n",
    "\n",
    "In this exercise, we will reproduce the analysis done during the tutorial with the connectome of cats but using connectivity data from humans. In this case, the network consist of a parcellation of the human cortex into **68 Regions of Interest** (ROIs)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# Load the SC networks for the 20 subjects\n",
    "dataroot = '../datasets/resting/Human/'\n",
    "scnets = load(dataroot + 'SCnets_20subs.npy')\n",
    "nsubs, N, N = shape(scnets)\n",
    "print( 'Number of subjects, nsubs = %d    Number of nodes, N = %d' %(nsubs, N) )\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Calculate and plot the population average SC matrix\n",
    "avscnet = scnets.mean(axis=0)\n",
    "\n",
    "# Symmetrise the weights and remove the diagonal entries (simplifies the graph analysis)\n",
    "netsym = 0.5*(avscnet + avscnet.T)\n",
    "for i in range(N):\n",
    "    netsym[i,i] = 0\n",
    "\n",
    "plt.figure()\n",
    "plt.imshow(netsym, cmap='gray_r')\n",
    "plt.clim(0,100000)\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# Fundamental properties of the network\n",
    "N = len(netsym)\n",
    "L = 0.5 * netsym.astype(bool).sum()\n",
    "Lmax = 0.5 * N*(N-1)\n",
    "dens = L / Lmax\n",
    "\n",
    "print('N: %d\\t\\tL: %d\\t\\tdensity: %1.3f' %(N, L, dens))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Well, the network seems too dense (1.0 !!) Meaning the many entries take a very small but non-zero value. Let's first plot the distribution and then binarise the network to simplify the graph analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# Get the non-zero weights and plot their distribution.\n",
    "nzidx = netsym.nonzero()\n",
    "scvalues = netsym[nzidx]\n",
    "\n",
    "plt.figure()\n",
    "plt.hist(scvalues, bins=100, density=True)\n",
    "plt.xscale('linear')\n",
    "plt.yscale('log')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What can we say from this distribution of link weights? Well, most of them are very weak. Although no the ultimate solution, we can safely discard the \"weakest\" connections and binarise this SC matrix. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# Binarise the SC network\n",
    "binet = where(netsym > 10000, 1, 0)\n",
    "binet = binet.astype(int)\n",
    "\n",
    "# Print out main properties\n",
    "L = 0.5 * binet.sum()\n",
    "Lmax = 0.5 * N*(N-1)\n",
    "dens = L / Lmax\n",
    "\n",
    "print('N: %d\\t\\tL: %d\\t\\tdensity: %1.3f \\n' %(N, L, dens))\n",
    "\n",
    "# And plot the adjacency matrix\n",
    "plt.figure()\n",
    "plt.imshow(binet,cmap='gray_r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<br/>\n",
    "### Local network properties (Exercise – 1)\n",
    "Let's start analysing the network. We will:\n",
    "1. find its degree distribution, and\n",
    "2. calculate the clustering coefficient\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "from galib import Degree, Clustering, Density\n",
    "\n",
    "# Compute the degree\n",
    "deg = Degree(binet, directed=False)\n",
    "print('Degrees: Min = %d, Max = %d, Mean degree: %1.3f' %(deg.min(), deg.max(), deg.mean()))\n",
    "\n",
    "# Plot the degree distribution\n",
    "plt.figure()\n",
    "plt.hist(deg, bins=10, range=(0,40), rwidth=0.8, density=True)\n",
    "plt.xlabel('Node degree', fontsize=14)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "C, Cnodes = Clustering(binet, checkdirected=False)\n",
    "\n",
    "print( 'Clustering coefficient: %1.3f' %C )\n",
    "print( 'Local clustering: Min = %1.3f, Max = %1.3f, Average = %1.3f' %(Cnodes.min(), Cnodes.max(), Cnodes.mean()) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Note\n",
    "\n",
    "*So far... does everything look correct until here? Can we say the threshold to binarise `netsym` (10,000) a good choice?*\n",
    "\n",
    "<br/>\n",
    "Complete the analysis from here, based on the analysis performed during the tutorial of the cat's corticortical network."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<br/>\n",
    "<br/>\n",
    "## Functional Connectivity –––––––––––––––––––––––––––––––––––––––––––––––\n",
    "\n",
    "Use the BOLD time-series to calculate the functional connectivity (cross-correlation matrix) for all subjects. Use function `corrcoef()` from NumPy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# Import the BOLD signals for all subjects\n",
    "dataroot = '../datasets/resting/Human/'\n",
    "bolds = load(dataroot + 'BOLD_20subs_Detrended.npy')\n",
    "nsubs, N, ntpoints = shape(bolds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# Compute the FC for all subjects and the average FC matrix\n",
    "fcnets = zeros((nsubs,N,N), float)\n",
    "for s in range(nsubs):\n",
    "    fcnets[s] = corrcoef(bolds[s])\n",
    "\n",
    "avfcnet = abs( fcnets.mean(axis=0) )\n",
    "\n",
    "# Plot the average FC\n",
    "plt.figure()\n",
    "plt.imshow(avfcnet)\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
