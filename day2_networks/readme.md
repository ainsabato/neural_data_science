### Course material for day 2.

Tutors, please upload here only the final material before the class.

---------
### NOTES

For the exercise with human connectivity data (ARCHI):

- Remove the diagonal entries from the SC matrices, e.g.:

```
for i in range(N):
	net[i,i] = 0
```

- The weighted SC matrices may not be fully symmetric. Symmetrise them before thresholding, e.g.:

```
netsym = 0.5*(scnet + scnet.T)
binet = where(netsym > 10000, 1, 0)
binet = binet.astype(int)
```
- Does the threshold leave ROIs disconnected? (Min degree=0) If so, decrease the threshold until the minimal degree is at least 1.

- For both the SC and the FC matrices, it might be easier to work with the population average matrices, rather than selecting an individual subject. Later, one can study individual deviations and cross subject variability. To get the averages:

```
avscnet = scnets.mean(axis=0)
netsym = 0.5 * (avscnet + avscnet.T)
```

- New version of the notebook for exercises has been uploaded, with the cleaning of the data simplified, to help focus on the network analysis: "*Notebook\_Exercise\_HumanConnectivity\_v2.ipynb*".

<br/>
Welcome to the annoying world of cleaning connectivity data.